﻿using BOWcfMailsAutomaticosProductos;
using DllWcfUtility;
using GeolocationWindowsTask.ServiceMailsB2B;
//using GeolocationWindowsTask.ServiceMailsB2B;
using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GeolocationWindowsTask
{
    public class GeolocationTask
    {
        private static ILog log = LogManager.GetLogger("FileLog");
        private static ILog logError = LogManager.GetLogger("FileLogError");

        static void Main(string[] args)
        {
            log.Info("GeolocationTask.Main Start");
            GoogleGeoCodeResponse response;
            WcfMailsAutomaticosB2BClient clientB2B;
            BeanList<GeolocationStatistics> statistics;
            Bean<GeolocationStatistics> statBean;
            string latitude;
            string longitude;
            int i;
            i = 0;
            try
            {
                
                
                clientB2B = new WcfMailsAutomaticosB2BClient();
                //response = GetGeocodeResponde(

                statistics = clientB2B.GetGeolocationData();

                if(statistics != null && statistics.beanList != null)
                {
                    log.Info("GeolocationTask.Main Actualizar estadísticas para " + statistics.beanList.Count + " datos");
                    foreach (GeolocationStatistics stat in statistics.beanList)
                    {
                        //GeolocationStatistics stat = new GeolocationStatistics();
                        //latitude = "23.0";//stat.Latitude;
                        //longitude = "-102.0";//stat.Longitude;
                        latitude = stat.Latitude;
                        longitude = stat.Longitude;
                        
                        log.Info("GeolocationTask.Main Actualizar estadísticas para latitud " + latitude + " longitud " + longitude);
                        i++;
                        //Console.WriteLine("Inicio: " + i + " de " + statistics.beanList.Count);
                        response = GetGeocodeResponde(latitude, longitude);
                        Thread.Sleep(200);
                        if (response != null && response.results.Length > 0)
                        {
                            foreach (results res in response.results)
                            {

                                CheckResult(res, stat);
                            }
                            Console.WriteLine("StreetNumber: " + stat.StreetNumber);
                            Console.WriteLine("Route: " + stat.Route);
                            Console.WriteLine("PostalCode: " + stat.PostalCode);
                            Console.WriteLine("Locality: " + stat.Locality);
                            Console.WriteLine("Country: " + stat.Country);
                            Console.WriteLine("AdminArea1: " + stat.AdminArea1);
                            Console.WriteLine("AdminArea2: " + stat.AdminArea2);
                            
                            //statBean = new Bean<GeolocationStatistics>();
                            //statBean.bean = stat;
                            //clientB2B.UpdateGeolocationData(statBean, 1);
                           
                           
                            
                        }
                        else
                        {
                            logError.Error("GeolocationTask.Main Error, no se han encontrado resultados para latitud " + latitude + " y longitud " + longitude);
                            //Console.ReadLine();
                        }
                        statBean = new Bean<GeolocationStatistics>();
                        statBean.bean = stat;
                        clientB2B.UpdateGeolocationData(statBean, 1);
                        //Console.WriteLine("Fin: " + i + " de " + statistics.beanList.Count);
                        //Console.WriteLine();
                    }
                }
                else
                {
          
                    logError.Error("GeolocationTask.Main Error recuperando datos de geolocalización");
                }
            }
            catch (Exception err)
            {
          
                logError.Error("GeolocationTask.Main Error: " + err.Message);
            }
          
            log.Info("GeolocationTask.Main End");
        }

        private static GoogleGeoCodeResponse GetGeocodeResponde(string ai_latitude, string ai_longitude)
        {
            log.Info("GeolocationTask.GetGeocodeResponde Start ai_latitude: " + ai_latitude + " ai_longitude: " + ai_longitude);
            GoogleGeoCodeResponse geoCodeResponse;
            geoCodeResponse = null;
            if (!string.IsNullOrEmpty(ai_latitude) && !string.IsNullOrEmpty(ai_longitude))
            {
                string address;
                System.Net.WebClient client;
                client = new System.Net.WebClient();
                client.Proxy = WebRequest.DefaultWebProxy;
                client.Credentials = System.Net.CredentialCache.DefaultCredentials; ;
                client.Proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
                client.Encoding = System.Text.Encoding.UTF8;
                address = string.Format(System.Configuration.ConfigurationManager.AppSettings.Get("GOOGLE_API_URL"), ai_latitude, ai_longitude);
                var result = client.DownloadString(address);
                client.Dispose();
                geoCodeResponse = JsonConvert.DeserializeObject<GoogleGeoCodeResponse>(result);
            }
            else
            {
                logError.Error("GeolocationTask.GetGeocodeResponde Error: latitud o longitud nula");
                geoCodeResponse = null;
            }
            log.Info("GeolocationTask.GetGeocodeResponde End");
            return geoCodeResponse;
        }

        private static void CheckResult(results ai_res, GeolocationStatistics ao_data)
        {

            if (ai_res != null)
            {
                foreach (address_component address in ai_res.address_components)
                {

                    switch (address.types[0])
                    {
                        case "street_number":
                            ao_data.StreetNumber = address.long_name;
                            break;
                        case "route":
                            ao_data.Route = address.long_name;
                            break;
                        case "locality":
                            ao_data.Locality = address.long_name;
                            break;
                        case "country":
                            ao_data.Country = address.long_name;
                            break;
                        case "postal_code":
                            ao_data.PostalCode = address.long_name;
                            break;
                        case "administrative_area_level_1":
                            ao_data.AdminArea1 = address.long_name;
                            break;
                        case "administrative_area_level_2":
                            ao_data.AdminArea2 = address.short_name + " - " + address.long_name;
                            break;
                    }
                }
            }
        }
    }

    public class GoogleGeoCodeResponse
    {

        public string status { get; set; }
        public results[] results { get; set; }

    }

    public class results
    {
        public string formatted_address { get; set; }
        public geometry geometry { get; set; }
        public string[] types { get; set; }
        public address_component[] address_components { get; set; }
    }

    public class geometry
    {
        public string location_type { get; set; }
        public location location { get; set; }
    }

    public class location
    {
        public string lat { get; set; }
        public string lng { get; set; }
    }

    public class address_component
    {
        public string long_name { get; set; }
        public string short_name { get; set; }
        public string[] types { get; set; }
    }
}
